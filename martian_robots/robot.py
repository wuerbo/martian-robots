ORIENTATION = ["N", "E", "S", "W"]


class RobotLost(Exception):

    def __init__(self, robot):
        self.robot = robot

    def __repr__(self):
        return "A robot has been lost at position {}, {}".format(self.robot.x, self.robot.y)


class Robot:
    ORIENTATION_OP = {
        'N': (0, 1),
        'S': (0, -1),
        'E': (1, 0),
        'W': (-1, 0)
    }

    def __init__(self, planet, init_x, init_y, orientation, instructions):
        self.planet = planet
        self.x = init_x
        self.y = init_y
        self.orientation = orientation
        self.instructions = instructions

    def execute_instructions(self):
        for instruction in self.instructions:
            instruction.execute(self)

    def change_orientation(self, direction):
        if direction == 'L':
            index_change_dir = -1
        elif direction == 'R':
            index_change_dir = 1
        new_index = ORIENTATION.index(self.orientation) + index_change_dir
        if new_index < 0:
            new_index = 3
        elif new_index > 3:
            new_index = 0
        self.orientation = ORIENTATION[new_index]

    def advance(self):
        new_x = self.x + self.ORIENTATION_OP.get(self.orientation)[0]
        new_y = self.y + self.ORIENTATION_OP.get(self.orientation)[1]

        if not self.planet.in_bounds(new_x, new_y) and not self.planet.grid[self.x][self.y].robot_scent:
            self.planet.grid[self.x][self.y].robot_scent = True
            raise RobotLost(self)
        elif self.planet.in_bounds(new_x, new_y):
            self.x = new_x
            self.y = new_y

    def __eq__(self, robot):
        return self.x == robot.x and self.y == robot.y and self.planet == robot.planet and self.orientation == robot.orientation and self.instructions == robot.instructions
