from robot_manager import RobotManager
from argparse import ArgumentParser


def init_args():

    parser = ArgumentParser(
        description='Martian-Robots: explore Mars!')
    parser.add_argument('input_file_path',
                        help='path for the input file')
    args = parser.parse_args()

    return args


if __name__ == "__main__":

    args = init_args()
    rm = RobotManager()
    rm.load_input(args.input_file_path)
    print(rm.execute_robots())
