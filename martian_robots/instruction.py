class Instruction:

    @staticmethod
    def execute(robot):
        pass


class LeftInstruction:
    @staticmethod
    def execute(robot):
        robot.change_orientation('L')


class RightInstruction:
    @staticmethod
    def execute(robot):
        robot.change_orientation('R')


class ForwardInstruction:
    @staticmethod
    def execute(robot):
        robot.advance()


INSTRUCTION_ALIAS = {
    'L': LeftInstruction,
    'R': RightInstruction,
    'F': ForwardInstruction,
}
