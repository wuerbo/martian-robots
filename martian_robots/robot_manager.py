from martian_robots.robot import Robot, RobotLost
from martian_robots.instruction import INSTRUCTION_ALIAS
from martian_robots.planet import Mars


class RobotManager:

    def __init__(self):
        self.robots = []

    def load_input(self, input_file_path):
        robots_info = []
        with open(input_file_path) as file:
            for i, line in enumerate(file.readlines()):
                if i == 0:  # 1st line must be grid bounds
                    x, y = line.split(' ')
                else:  # each two next lines must be information about a new robot
                    if i % 2 == 1:  # 1st part should be initial position
                        initx, inity, orientation = line.split(' ')
                        robots_info.append((initx, inity, orientation.strip()))
                    elif i % 2 == 0:  # read instructions
                        instructions = [INSTRUCTION_ALIAS.get(
                            char) for char in line if char in INSTRUCTION_ALIAS]  # only parse valid instructions
                        # add it to the already included info of the robot
                        robots_info[-1] = robots_info[-1] + (instructions, )

        planet = Mars(int(x), int(y))
        self.robots = [
            Robot(planet, int(init_x), int(init_y), orientation, instructions) for init_x, init_y, orientation, instructions in robots_info
        ]

    def execute_robots(self):
        result = ''
        for robot in self.robots:
            try:
                robot.execute_instructions()
                result += '{} {} {}\n'.format(robot.x,
                                              robot.y, robot.orientation)

            except RobotLost as rbt_exc:
                result += '{} {} {} {}\n'.format(rbt_exc.robot.x,
                                                 rbt_exc.robot.y, rbt_exc.robot.orientation, 'LOST')
        return result

    def __eq__(self, robot_manager):
        return self.robots == robot_manager.robots
