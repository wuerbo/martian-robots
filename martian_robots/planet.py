class Cell:

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.robot_scent = False

    def __repr__(self):
        return "| X:{} Y:{} - {} |".format(self.x, self.y, 'X' if self.robot_scent else 'O')

    def __eq__(self, cell):
        return self.x == cell.x and self.y == cell.y and self.robot_scent == cell.robot_scent


class Mars:
    MAX_COORDINATE_VALUE = 50

    def __init__(self, top_bound_x, top_bound_y):
        self.grid = []
        if 0 < top_bound_x <= self.MAX_COORDINATE_VALUE and 0 < top_bound_y <= self.MAX_COORDINATE_VALUE:
            for i in range(top_bound_x + 1):
                self.grid.append([])
                for j in range(top_bound_y + 1):
                    self.grid[i].append(Cell(i, j))
        else:
            raise ValueError

    def in_bounds(self, posx, posy):
        return posx in range(len(self.grid)) and posy in range(len(self.grid[0]))

    def __eq__(self, planet):
        for row1, row2 in zip(self.grid, planet.grid):
            for i, j in zip(row1, row2):
                if i != j:
                    return False
        return True
