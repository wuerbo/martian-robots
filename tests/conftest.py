import pytest
import os

from martian_robots.instruction import RightInstruction, LeftInstruction, ForwardInstruction
from martian_robots.planet import Mars
from martian_robots.robot import Robot


@pytest.fixture
def sample_robots1():
    planet = Mars(5, 3)
    return [
        Robot(planet, 1, 1, "E", [
            RightInstruction, ForwardInstruction, RightInstruction, ForwardInstruction, RightInstruction, ForwardInstruction, RightInstruction, ForwardInstruction
        ]),
        Robot(planet, 3, 2, "N", [
            ForwardInstruction, RightInstruction, RightInstruction, ForwardInstruction, LeftInstruction, LeftInstruction, ForwardInstruction, ForwardInstruction, RightInstruction, RightInstruction, ForwardInstruction, LeftInstruction, LeftInstruction
        ]),
        Robot(planet, 0, 3, "W", [
            LeftInstruction, LeftInstruction, ForwardInstruction, ForwardInstruction, ForwardInstruction, LeftInstruction, ForwardInstruction, LeftInstruction, ForwardInstruction, LeftInstruction
        ])
    ]


@pytest.fixture
def sample_planets():
    return [Mars(5, 3), Mars(10, 10), Mars(50, 50)]


@pytest.fixture
def sample_file_path():
    test_dir = os.path.dirname(os.path.realpath(__file__))
    root_dir = os.path.abspath(os.path.join(test_dir, os.pardir))
    return os.path.join(root_dir, 'resources', 'sample_input.txt')
