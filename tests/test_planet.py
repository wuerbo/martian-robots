
def test_in_bounds(sample_planets):
    mars1, mars2, mars3 = sample_planets
    # 5-3  10-10  50-50
    test_point = -1, 3
    assert not mars1.in_bounds(test_point[0], test_point[1])
    test_point = 6, 3
    assert not mars1.in_bounds(test_point[0], test_point[1])
    test_point = 1, 3
    assert mars1.in_bounds(test_point[0], test_point[1])
    test_point = 0, 0
    assert mars1.in_bounds(test_point[0], test_point[1])
    test_point = 5, 3
    assert mars1.in_bounds(test_point[0], test_point[1])

    test_point = -1, 3
    assert not mars2.in_bounds(test_point[0], test_point[1])
    test_point = 11, 10
    assert not mars2.in_bounds(test_point[0], test_point[1])
    test_point = 8, 9
    assert mars2.in_bounds(test_point[0], test_point[1])
    test_point = 0, 0
    assert mars2.in_bounds(test_point[0], test_point[1])
    test_point = 10, 10
    assert mars2.in_bounds(test_point[0], test_point[1])

    test_point = -1, 3
    assert not mars3.in_bounds(test_point[0], test_point[1])
    test_point = 70, 50
    assert not mars3.in_bounds(test_point[0], test_point[1])
    test_point = 43, 23
    assert mars3.in_bounds(test_point[0], test_point[1])
    test_point = 0, 0
    assert mars3.in_bounds(test_point[0], test_point[1])
    test_point = 50, 50
    assert mars3.in_bounds(test_point[0], test_point[1])
