from martian_robots.robot_manager import RobotManager


def test_load_input(sample_robots1, sample_file_path):
    rm1 = RobotManager()
    rm1.robots = sample_robots1
    rm2 = RobotManager()
    rm2.load_input(sample_file_path)
    assert rm1 == rm2


def test_execute_robots(sample_robots1):
    rm1 = RobotManager()
    rm1.robots = sample_robots1
    result = rm1.execute_robots()
    assert result == "1 1 E\n3 3 N LOST\n2 3 S\n"
