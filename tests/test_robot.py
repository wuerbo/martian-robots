import pytest
from martian_robots.robot import RobotLost


def test_change_orientation(sample_robots1):
    robot1, robot2, _, = sample_robots1

    robot1.change_orientation('R')
    assert robot1.orientation == 'S'
    robot1.change_orientation('L')
    assert robot1.orientation == 'E'
    robot2.change_orientation('L')
    assert robot2.orientation == 'W'
    robot2.change_orientation('R')
    assert robot2.orientation == 'N'


def test_advance(sample_robots1):
    robot1, robot2, _, = sample_robots1

    robot1.advance()
    assert robot1.x == 2 and robot1.y == 1
    robot1.advance()
    assert robot1.x == 3 and robot1.y == 1
    robot1.change_orientation('R')
    robot1.advance()
    assert robot1.x == 3 and robot1.y == 0

    with pytest.raises(RobotLost) as excinfo:
        robot1.advance()
        assert "A robot has been lost at position 3, 0" in str(excinfo.value)

    with pytest.raises(RobotLost) as excinfo:
        robot2.execute_instructions()
        assert "A robot has been lost at position 3, 3" in str(excinfo.value)
