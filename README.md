## Installation
The root directory needs to be added to PYTHONPATH variable. If the project directory is in foo/bar/martian-robots folder, for Linux:
`
export PYTHONPATH='foo/bar/martian-robots'
`

Windows:
`
set PYTHONPATH=%PYTHONPATH%;C:\foo\bar\martian-robots
`

In your environment of choice (pipenv, venv, etc) install requirements:

`
pip install -r requirements.txt
`

## Execution
`
python martian_robots/main.py input_file_path
`

In resources there is a sample input:

`
python martian_robots/main.py resources/sample_input.txt
`

## Tests
`
pytest
`